// Flutter imports:
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

configureEnvironmentBeforeStart() async {
  WidgetsFlutterBinding.ensureInitialized();
}
