import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen.dart';
import 'package:ideal_atm/main/presentation/utils/themes.dart';

void screenTests(Stopwatch stopwatch) {
  group('GoldenScreenTests', () {
    _createGroupElement(
      screenName: 'Home Screen',
      fileName: 'screens/home_screen',
      screen: const HomeScreen(),
      stopwatch: stopwatch,
    );
  });
}

void _createGroupElement({
  required String screenName,
  required String fileName,
  required Widget screen,
  required Stopwatch stopwatch,
}) async {

  testGoldens(screenName, (WidgetTester tester) async {
    debugPrint('Golden test for light theme started in ${stopwatch.elapsed}');
    DeviceBuilder builder = DeviceBuilder();
    debugPrint('Device Builder setup in ${stopwatch.elapsed}');
    builder.addScenario(widget: SizedBox.shrink(child: screen));
    debugPrint('Scenario added in ${stopwatch.elapsed}');
    await tester.pumpDeviceBuilder(builder,
        wrapper: materialAppWrapper(theme: themeLight()));
    debugPrint('Screens build in ${stopwatch.elapsed}');
    await screenMatchesGolden(
      tester,
      '${fileName}_light',
    );
    debugPrint('Golden test for light theme finished in ${stopwatch.elapsed}');
  });

  testGoldens(screenName, (WidgetTester tester) async {
    debugPrint('Golden test for light theme exited in ${stopwatch.elapsed}');
    DeviceBuilder builder = DeviceBuilder();
    builder.addScenario(widget: SizedBox.shrink(child: screen));
    await tester.pumpDeviceBuilder(builder,
        wrapper: materialAppWrapper(theme: themeDark()));

    await screenMatchesGolden(
      tester,
      "${fileName}_dark",
    );
    debugPrint('Dark tests executed in ${stopwatch.elapsed}');
  });
}
