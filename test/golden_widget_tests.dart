import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/news_widget.dart';

void widgetTests() {
  _createGroupElement(
      element: 'News Widget',
      fileName: 'widgets/news_widget',
      widgets: [
        const NewsWidget(
          title: 'Переходите на снапшот тестирование уже сегодня!',
          icon: Icons.warning,
        ),
        const NewsWidget(
          title: 'Славьте солнце!',
          icon: Icons.accessibility_new,
        )
      ]);
  _createGroupElementFontScale(
    element: 'News Widget Fonts',
    fileName: 'widgets/news_widget_fonts',
    widget: const NewsWidget(
      title: 'Переходите на снапшот тестирование уже сегодня!',
      icon: Icons.warning,
    ),
  );
}

void _createGroupElement({
  required String element,
  required String fileName,
  required List<Widget> widgets,
  Size size = const Size(400, 350),
}) async {
  testGoldens(
    element,
    (WidgetTester tester) async {
      GoldenBuilder builder = GoldenBuilder.column();

      for (Widget widget in widgets) {
        final widgetName = widget.toString();
        builder.addScenario(
          widgetName,
          widget,
        );
      }
      await tester.pumpWidgetBuilder(
        builder.build(),
        surfaceSize: size,
        textScaleSize: 1.0,
      );
      await screenMatchesGolden(tester, fileName);
    },
  );
}

void _createGroupElementFontScale({
  required String element,
  required String fileName,
  required Widget widget,
  Size size = const Size(400, 1250),
}) async {
  testGoldens(
    element,
    (WidgetTester tester) async {
      GoldenBuilder builder = GoldenBuilder.column();
      builder
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 0.8)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 1.0)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 1.2)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 1.4)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 1.6)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 1.8)
        ..addTextScaleScenario('Font Scale:', widget, textScaleFactor: 2.0);
      await tester.pumpWidgetBuilder(
        builder.build(),
        surfaceSize: size,
      );
      await screenMatchesGolden(tester, fileName);
    },
  );
}
