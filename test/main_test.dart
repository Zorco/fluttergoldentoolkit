import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'golden_screen_tests.dart' as screen_tests;
import 'golden_widget_tests.dart' as widget_tests;
import 'configure_environment.dart' as setup;
void main() async {
  Stopwatch stopwatch = Stopwatch()..start();
  setup.configureEnvironmentBeforeStart();
  debugPrint('All test setup executed in ${stopwatch.elapsed}');
  group('all tests /', () {
    screen_tests.screenTests(stopwatch);
    widget_tests.widgetTests();
  });
}
