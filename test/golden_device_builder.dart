// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:golden_toolkit/golden_toolkit.dart';

class GoldenSetup {
  static List<Device> devices = [
    const Device(
      size: Size(390, 844),
      name: 'iPhone_14',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 54, 0, 34),
    ),
    const Device(
      size: Size(411, 914),
      name: 'Pixel_6',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(320, 568),
      name: 'iPhone_SE',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 40, 0, 0),
    ),
    const Device(
      size: Size(768, 1076),
      name: 'Samsung_Fold',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(375, 812),
      name: 'iPhone 10',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 44, 0, 34),
    ),
    const Device(
      size: Size(1112, 834),
      name: 'iPad Air ',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(412, 915),
      name: 'OnePlus 8',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(412, 892),
      name: 'Samsung A30',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(412, 1004),
      name: 'Samsung Z Flip',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
    const Device(
      size: Size(540, 960),
      name: 'Sony Xperia Z Ultra',
      devicePixelRatio: 1.0,
      textScale: 1.0,
      brightness: Brightness.light,
      safeArea: EdgeInsets.fromLTRB(0, 0, 0, 0),
    ),
  ];
}
