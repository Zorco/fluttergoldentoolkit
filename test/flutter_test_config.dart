// Dart imports:
import 'dart:async';
import 'dart:io';

// Package imports:
import 'package:golden_toolkit/golden_toolkit.dart';

// Project imports:
import 'golden_device_builder.dart';

Future<void> testExecutable(FutureOr<void> Function() testMain) async {
  return GoldenToolkit.runWithConfiguration(
        () async {
      await loadAppFonts();
      await testMain();
    },
    config: GoldenToolkitConfiguration(
      skipGoldenAssertion: () => !Platform.isMacOS,
      defaultDevices: GoldenSetup.devices,
    ),
  );
}
