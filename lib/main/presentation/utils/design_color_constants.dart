import 'package:flutter/material.dart';

Color primaryLight = const Color(0xFF5DB8EB);
Color onPrimaryLight = const Color(0xFF000000);
Color primaryContainerLight = const Color(0xFFFFF5F5);
Color onPrimaryContainerLight = const Color(0xFFA9A8A8);
Color secondaryLight = const Color(0xFFB6D1E8);
Color onSecondaryLight = const Color(0xFF000000);
Color secondaryContainerLight = const Color(0xFF60B8CC);
Color onSecondaryContainerLight = const Color(0xFF3E7992);
Color surfaceLight = const Color(0xFFCEEAF0);
Color onSurfaceLight = const Color(0xFF000000);
Color tertiaryLight = const Color(0xFFFFFFFF);
Color onTertiaryLight = const Color(0xFF000000);
Color outlineLight = const Color(0xED062436);

Color primaryDark = const Color(0xFF2687BC);
Color onPrimaryDark = const Color(0xFFFFFFFF);
Color primaryContainerDark = const Color(0xFF2B292F);
Color onPrimaryContainerDark = const Color(0xFFA9A8A8);
Color secondaryDark = const Color(0xFF2B292F);
Color onSecondaryDark = const Color(0xFFFFFFFF);
Color secondaryContainerDark = const Color(0xFF2C2D31);
Color onSecondaryContainerDark = const Color(0xFFFFFBFB);
Color surfaceDark = const Color(0xFF333A48);
Color onSurfaceDark = const Color(0xFFFFFFFF);
Color tertiaryDark = const Color(0xFF223E42);
Color onTertiaryDark = const Color(0xFFFFFFFF);
Color outlineDark = const Color(0xFF8FB2F1);
