enum NavPages {
  homeScreen,
  findATMScreen,
  qrScreen,
  payments,
  userProfileScreen,
  authScreen,
  regScreen
}
