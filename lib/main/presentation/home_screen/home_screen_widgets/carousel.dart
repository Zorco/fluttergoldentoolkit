import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ideal_atm/core/constants/app_assets.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/carousel_button_widget.dart';
import 'package:ideal_atm/main/presentation/utils/design_widget_constants.dart';

class CarouselWidget extends StatelessWidget {
  const CarouselWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> buttonsList = [
      const CarouselButtonWidget(
        title: 'Маркетплейс',
        image: AppAssets.cartIcon,
      ),
      const CarouselButtonWidget(
        title: "Конвертация",
        image: AppAssets.conversionIcon,
      ),
      const CarouselButtonWidget(
        title: "Налоги",
        image: AppAssets.payments1Icon,
      ),
      const CarouselButtonWidget(
        title: "Платежи",
        image: AppAssets.payments2Icon,
      ),
    ];
    return CarouselSlider(
      options: CarouselOptions(
        viewportFraction: carouselWidgetViewportFraction,
        height: carouselWidgetHeight,
        scrollDirection: Axis.horizontal,
      ),
      items: buttonsList,
    );
  }
}
