import 'package:flutter/material.dart';
import 'package:ideal_atm/core/constants/app_assets.dart';
import 'package:ideal_atm/main/presentation/utils/design_widget_constants.dart';

class CardADWidget extends StatelessWidget {
  const CardADWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Align(
      alignment: Alignment.center,
      child: ADWidget(),
    );
  }
}

class ADWidget extends StatelessWidget {
  const ADWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    return Padding(
      padding: adWidgetPadding,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(adWidgetBorderCropRadius),
          child: Image.asset(brightness == Brightness.dark
              ? AppAssets.adPromoDark
              : AppAssets.cage)),
    );
  }
}
