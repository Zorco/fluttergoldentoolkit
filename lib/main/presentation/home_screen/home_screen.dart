import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/card_ad_widget.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/carousel.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/ideal_bank_logo.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen_widgets/news_feed_widget.dart';
import 'package:ideal_atm/main/presentation/widgets/app_widgets/app_bar.dart';
import 'package:ideal_atm/main/presentation/widgets/app_widgets/custom_nav_bar.dart';

import 'navigatable_pages.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: CustomAppBar(
        elevation: 0,
        isPoppable: false,
      ),
      floatingActionButton: QRScreenFloatingActionButton(),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavBar(
        navPage: NavPages.homeScreen,
        isFABPresent: true,
      ),
      extendBody: true,
      body: Column(
        children: [
          BankLogo(),
          CardADWidget(),
          CarouselWidget(),
          NewsFeedWidget()
        ],
      ),
    );
  }
}
