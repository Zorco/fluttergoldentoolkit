import 'package:flutter/material.dart';
import 'package:ideal_atm/main/presentation/home_screen/home_screen.dart';
import 'package:ideal_atm/main/presentation/utils/themes.dart';

Future<void> main() async {
  runApp(const IdealAtm(screen: HomeScreen()));
}

class IdealAtm extends StatelessWidget {
  const IdealAtm({Key? key, required this.screen}) : super(key: key);
  final Widget screen;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scrollBehavior: const ScrollBehavior(),
      home: screen,
      debugShowCheckedModeBanner: false,
      theme: themeLight(),
      darkTheme: themeDark(),
      routes: const {},
    );
  }
}
