import 'package:flutter/material.dart';

class AppStyles {
  static const s12w500 = TextStyle(
    fontFamily: 'MetaPro',
    fontWeight: FontWeight.w500,
    fontSize: 12,
    letterSpacing: 0.3,
  );

  static const s14w400 = TextStyle(
    fontFamily: 'MetaPro',
    fontSize: 14,
    fontWeight: FontWeight.w400,
    decorationThickness: 1.5,
    color: Colors.white,
  );
  static const s14w600 = TextStyle(
      fontFamily: 'MetaPro', fontSize: 14, fontWeight: FontWeight.w600);

  static const s16w700 = TextStyle(
    fontFamily: 'MetaPro',
    fontWeight: FontWeight.w700,
    fontSize: 16,
    letterSpacing: 0.3,
  );
  static const s16w500 = TextStyle(
    fontFamily: 'MetaPro',
    fontWeight: FontWeight.w500,
    fontSize: 16,
    letterSpacing: 0.3,
  );

  static const s18w600 = TextStyle(
    fontFamily: 'MetaPro',
    fontWeight: FontWeight.w600,
    fontSize: 18,
    letterSpacing: 0.3,
  );
}
