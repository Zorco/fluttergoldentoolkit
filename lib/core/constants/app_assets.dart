class AppAssets {
  // PNGs
  static const String backGround = 'assets/images/.png';
  static const String photo = 'assets/images/photo.png';
  static const String card = 'assets/images/credit_card.png';
  static const String convert = 'assets/images/convert.png';
  static const String convertNew = 'assets/images/convert_new.png';
  static const String savings = 'assets/images/savings.png';
  static const String online = 'assets/images/online.png';
  static const String coin = 'assets/images/coin.png';
  static const String marketplace = 'assets/images/marketplace.png';
  static const String sales = 'assets/images/sales.png';
  static const String bank = 'assets/images/bank_card.png';
  static const String logo = 'assets/images/bank_logo.png';
  static const String atmImage = 'assets/images/tinkoff.png';
  static const String map = 'assets/images/map.png';
  static const String adPromoLight = 'assets/images/ad_promo_light.png';
  static const String adPromoDark = 'assets/images/ad_promo_dark.png';
  static const String atms = 'assets/images/atm.jpg';
  static const String avatar = 'assets/images/avatar.png';
  static const String cage = 'assets/images/cage.jpeg';
  // SVGs
  static const String placeLocation = 'assets/icons/placelocation_marker.svg';
  static const String myLocation = 'assets/icons/mylocation_marker.svg';
  static const String atm = 'assets/icons/atm_logo.svg';
  static const String homeIcon = 'assets/icons/home_icon.svg';
  static const String atmIcon = 'assets/icons/atm_icon.svg';
  static const String cartIcon = 'assets/icons/cart.svg';
  static const String conversionIcon = 'assets/icons/conversion.svg';
  static const String langIcon = 'assets/icons/international.svg';
  static const String mapIcon = 'assets/icons/map_icon.svg';
  static const String optionsIcon = 'assets/icons/options.svg';
  static const String payments1Icon = 'assets/icons/payments1.svg';
  static const String payments2Icon = 'assets/icons/payments2.svg';
  static const String profileIcon = 'assets/icons/profile.svg';
  static const String qrCodeIcon = 'assets/icons/qr_code.svg';
  static const String arrowRoundedIcon = 'assets/icons/arrow_rounded.svg';
  static const String arrowSharpIcon = 'assets/icons/arrow_sharp.svg';
  static const String idealBankLogoSmall =
      'assets/icons/ideal_bank_logo_small.svg';
  static const String idealBankLogoBig = 'assets/icons/ideal_bank_logo_big.svg';
  // PDFs
  static const String personalDataAgreementRU = 'assets/pdfs/pers_data_ru.pdf';
  static const String personalDataAgreementKZ = 'assets/pdfs/pers_data_kz.pdf';
  static const String personalDataAgreementEN = 'assets/pdfs/pers_data_en.pdf';
}
