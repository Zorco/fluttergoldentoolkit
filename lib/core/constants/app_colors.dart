import 'dart:ui';

class AppColors {
  //Basic
  static const white = Color(0xffFFFFFF);
  static const black = Color(0xff000000);
  static const transparent = Color(0x00000000);
  static const almostTransparent = Color(0x30f5f5f5);

  //App palette
  static const bgColor = Color(0xff099ebf);
  static const grey = Color(0xffa9a9a9);
  static const greyLight = Color(0xffBCC2CC);
  static const blueDark = Color(0xff31456A);
  static const blueLight = Color(0xff0093BF);
  static const hintColor = Color.fromRGBO(117, 133, 161, 1);
  static const yellow = Color(0xfff0cf0e);
  static const black2 = Color(0xff2c3131);
  static const darkGrey = Color(0xff626262);

  //Gradient
  static const gradientEnd = Color(0xffa9a9a9);
  static const gradientBegin = Color(0xffc4cccc);
}
